$(document).ready(function () {

    var token;
    var activeTable
    if (window.location.pathname !== "/" && localStorage.getItem('data')) {
        token = localStorage.getItem('data').token;
        let expiry = localStorage.getItem('data').timestamp;

        if (new Date().getTime() - expiry >= 3600000) {
            localStorage.removeItem('data');
            window.location.href = '/';
        }
    } else if (window.location.pathname !== "/") {
        window.location.href = '/';
    }

    $("#sidebarCollapse").on("click", function () {
        $("#sidebar").toggleClass("active");
    });

    let baseUrl = "/api/v1/transaction/";
    let table = [
        "adminusers",
        "adminuserlogs",
        "childactivities",
        "childassessments",
        "childnurtureassessments",
        "childreminders",
        "children",
        "featuredimages",
        "piausers",
        "useractivities",
        "userarticles",
        "userassessments",
        "userdevicedetails",
        "useremailotps",
        "userfeedbacks",
        "userotps",
        "users"
    ];

    table.sort()

    for (i = 0; i < table.length; i++) {
        $("ul#tableHeaders").append(
            '<li id="' +
            table[i] +
            '"><a href="#' +
            table[i] +
            '"><span class="tab" style="color:white;">' +
            table[i] +
            "</span></a></li>"
        );

        $("div#mainTableContent").append(
            '<section id="' +
            table[i] +
            '" class="tableContent"><h3 class="text-center" style="padding-bottom:10px;">' +
            table[i].toUpperCase() +
            ' Table</h4><table id="' +
            table[i] +
            'Table" class="table table-hover table-bordered display cmsTable"></table></section>'
        );
    }

    $("#mainTableContent")
        .children()
        .css("display", "none");

    $("#tableHeaders li").click(function (e) {
        e.preventDefault();
        $('.pagination-button').removeClass('hidden')
        activeTable =
            table[
                $(this)
                .closest("li")
                .index()
            ];
        let page = 1;
        tableData(activeTable, page)
    });


    $('.submit-btn').click(function (e) {
        e.preventDefault();

        if (!$('#email').val()) {
            alert("Enter email id");
            return false;
        }

        if (!$('#pwd').val()) {
            alert("Enter password");
            return false;
        }

        let jsonData = {
            email: $('#email').val(),
            password: $('#pwd').val()
        };

        $.ajax({
            url: "http://localhost:3000/api/v1/user/authenticate/admin",
            type: "post",
            contentType: 'application/json',
            data: JSON.stringify(jsonData),
            success: function (data) {
                localStorage.setItem('data', JSON.stringify({
                    "token": data.data.apiToken,
                    "timestamp": new Date().getTime()
                }));

                window.location.href = '/dashboard';
            },
            error: function (jqxhr) {
                console.log(JSON.parse(jqxhr.responseText).error.message);
                alert(JSON.parse(jqxhr.responseText).error.message);
            }
        });

    });

    $('#logout').click(function (e) {
        localStorage.removeItem('data');
        window.location.href = '/';
    });

    function tableData(activeTable, page) {

        $("#" + activeTable + "Table").empty();

        if (localStorage.getItem('data')) {
            let expiry = JSON.parse(localStorage.getItem('data')).timestamp;

            if (new Date().getTime() - expiry >= 3600000) {
                localStorage.removeItem('data');
            }
        } else {
            window.location.href = '/';
        }

        $(".content-heading").css("display", "none");

        $("ul#tableHeaders > li").css("background", "#0957A5");

        $("ul#tableHeaders > li").find('span.tab').css("color", "white");

        $("#mainTableContent")
            .children()
            .css("display", "none");

        $(this)
            .closest("li")
            .css("background", "white");

        $(this)
            .closest("li").find('span.tab')
            .css("color", "#0957A5");

        // let activeTable =
        //     table[
        //         $(this)
        //         .closest("li")
        //         .index()
        //     ];

        $.ajax({
            url: baseUrl + activeTable + "?page=" + page,
            headers: {
                'x-access-token': JSON.parse(localStorage.getItem('data')).token
            },
            success: function (data) {
                $('.pageNoLabel').text("");
                if (data.data.result.length == 0 || typeof data.data.result == "string") {
                    alert("no data found");
                } else {

                    for (let i = 0; i <= parseInt(data.data.count / 2000); i++) {
                        if (parseInt(page) === (i + 1)) {
                            $(".pageNoLabel").append('<span class = "pagination-pageNo" style="cursor:pointer; background-color:#66e0ff">' + (i + 1) + '</span>');
                        } else {
                            $(".pageNoLabel").append('<span class = "pagination-pageNo" style="cursor:pointer;background-color:white"">' + (i + 1) + '</span>');
                        }
                    }
                    let columnHeads = [];
                    let columnDefs = [{
                        "defaultContent": "-",
                        "targets": "_all",
                        searchable: true
                    }];

                    $("table#" + activeTable + "Table").append("<thead><tr>");
                    var objIndex = 0;


                    let maxObjectIndex = 0;
                    let objectLength = 0;

                    data.data.result.map((obj, index) => {
                        if (Object.keys(obj).length > objectLength) {
                            objectLength = Object.keys(obj).length;
                            maxObjectIndex = index;
                        }
                    });

                    for (var key in data.data.result[maxObjectIndex]) {
                        $("table#" + activeTable + "Table").append(
                            '<th class="text-center">' + key + "</th>"
                        );

                        columnHeads.push({
                            data: key
                        });

                        // data =
                        //   '<a href="api/v1/' +
                        //   Object.keys(row)[meta.col] +
                        //   "/" +
                        //   data +
                        //   '" target="_blank">' +
                        //   data +
                        //   "</a>";

                        if (
                            Array.isArray(data.data.result[0][key]) &&
                            data.data.result[maxObjectIndex][key].length
                        ) {
                            columnDefs.push({
                                "type": "html",
                                targets: objIndex,
                                "className": "text-center",
                                "width": "4%",
                                "searchable": true,
                                render: function (data, type, row, meta) {
                                    let dataName = [];
                                    let searchString = [];
                                    if (type === "display") {
                                        for (let i = 0; i < data.length; i++) {
                                            let updateValue;
                                            if (data[i]) {
                                                updateValue = `${data[i].name?data[i].name:data[i].response?data[i].response:data[i].user && data[i].user.firstName?data[i].user.firstName:data[i].child && data[i].child.firstName?data[i].child.firstName:data[i].firstName?data[i].firstName:typeof data[i] === "string"?data[i]:""}`;
                                                dataName.push(`<span class="content-id" title=${data[i]._id}>${updateValue}</span>`);
                                            } else if (data[i]._id) {
                                                updateValue = data[i]._id;
                                                dataName.push(data[i]._id);
                                            } else {
                                                updateValue = data[i];
                                                dataName.push(data[i])
                                            }
                                            searchString.push(updateValue);
                                        }
                                    }

                                    meta.settings.aoData[meta.row]._aFilterData ? meta.settings.aoData[meta.row]._aFilterData[meta.col] = searchString.toString() : "";

                                    meta.settings.aoData[meta.row]._sFilterRow ? meta.settings.aoData[meta.row]._sFilterRow = meta.settings.aoData[meta.row]._aFilterData.toString() : meta.settings.aoData[meta.row]._sFilterRow


                                    return dataName;
                                }
                            });
                        } else if (typeof data.data.result[maxObjectIndex][key] === "object") {
                            columnDefs.push({
                                "type": "html",
                                targets: objIndex,
                                "searchable": true,
                                "className": "text-center",
                                "width": "4%",
                                render: function (data, type, row, meta) {
                                    let dataName;
                                    if (type === "display") {
                                        if (data) {
                                            dataName = `<span class="content-id" title=${data._id}>${data.name?data.name:data.firstName?data.firstName:data.email?data.email:data}</span>`;
                                        } else if (data && data._id) {
                                            dataName = data._id;
                                        } else {
                                            dataName = data;
                                        }
                                    }

                                    meta.settings.aoData[meta.row]._aFilterData ? meta.settings.aoData[meta.row]._aFilterData[meta.col] = dataName : "";

                                    meta.settings.aoData[meta.row]._sFilterRow ? meta.settings.aoData[meta.row]._sFilterRow = meta.settings.aoData[meta.row]._aFilterData.toString() : meta.settings.aoData[meta.row]._sFilterRow

                                    return dataName;
                                }
                            });
                        } else {
                            columnDefs.push({
                                targets: objIndex,
                                "searchable": true,
                                "className": "text-center",
                                "width": "4%"
                            });
                        }

                        objIndex++;
                    }

                    $("table#" + activeTable + "Table").append(
                        "</tr></thead><tbody></tbody>"
                    );

                    $("section#" + activeTable + "").css("display", "block");
                    var dt = $("#" + activeTable + "Table").DataTable({
                        processing: true,
                        data: data.data.result,
                        columns: columnHeads,
                        columnDefs: columnDefs,
                        "dom": '<lfi<t>p>',
                        bDestroy: true
                    });

                    $("#" + activeTable + "Table").on('search.dt', function () {
                        var value = $("#" + activeTable + "Table_filter input").val();
                        $("#" + activeTable + "Table").removeHighlight();
                        $("#" + activeTable + "Table").highlight(value);
                    });
                }
            },
            error: function (jqxhr) {
                console.log(jqxhr.responseText);
                alert(jqxhr.responseText);
            }
        });
    }

    $("li.pageNoLabel").delegate("span.pagination-pageNo", "click", function () {


        let pageNo = $(this).text();

        //$(this).css("background-color", "#66e0ff");

        tableData(activeTable, pageNo)
    });

});