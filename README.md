Note:- Javascript handles timezone with there own. If we send timestamp from backend, Javascript automatically check the system timezone and convert the timestamp into system time zone.

# Appointments Available

## Project setup

# npm install

## To run this project

# npm run dev

## Routings

# POST /api/v1/appointment/ -> To Store availability timing

payload = {
"startTime":1596823200000,
"endTime":1596826800000
}

reponse = {
"statusCode": 201,
"data": {
"\_id": "5f2b058d77a2087ffe529b92",
"startTime": "2020-08-07T18:00:00.000Z",
"endTime": "2020-08-07T19:00:00.000Z",
"createdAt": "2020-08-05T19:16:29.145Z",
"updatedAt": "2020-08-05T19:16:29.145Z",
"\_\_v": 0
},
"error": {}
}

# POST /api/v1/appointment/availability -> To get availability timing for a day

payload = {
"appointmentDay":1596758400000
}

response = {
"statusCode": 201,
"data": [
{
"startTime": 1596823200000,
"endTime": 1596826800000
}
],
"error": {}
}

# GET /api/v1/appointment/all -> To get all availability timing

response = {
"statusCode": 201,
"data": [
{
"\_id": "2020-08-07",
"appointments": [
{
"startTime": 1596823200000,
"endTime": 1596826800000
}
]
}
],
"error": {}
}
