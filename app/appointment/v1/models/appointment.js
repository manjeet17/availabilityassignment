import mongoose from 'mongoose';
const Schema = mongoose.Schema;
var appointmentSchema = new Schema({
    startTime: Date,
    endTime: Date,
    appointmentDay: Date
}, {
    timestamps: true
});

export default mongoose.model('Appointment', appointmentSchema);