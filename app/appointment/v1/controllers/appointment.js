import config from '../../../config';

import {
    getAvailableDate
} from "../../../utils/v1/appointment-util";

const Appointment = require('mongoose').model('Appointment');

export async function addAppointment(req, res) {
    const requestBody = req.body;

    if (!requestBody.startTime || !requestBody.endTime) {
        return res.validationFailed("Parameters are missing");
    }

    try {

        if (new Date(requestBody.startTime).getTime() >= new Date(requestBody.endTime).getTime()) {
            return res.validationFailed("End time should be greater than start time");
        }

        let appointment = await new Appointment({
            startTime: new Date(requestBody.startTime),
            endTime: new Date(requestBody.endTime)
        }).save();

        res.successWithData(appointment);

    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}

export async function getAppointmentForDay(req, res) {
    const requestBody = req.body;

    try {

        let endDate = new Date(requestBody.appointmentDay);

        endDate = endDate.setDate(endDate.getDate() + 1)

        let appointments = await Appointment.find({
            startTime: {
                $gte: new Date(requestBody.appointmentDay),
                $lt: endDate
            }
        }).sort({
            startTime: 1
        });

        let appointmentDate = getAvailableDate(appointments)

        res.successWithData(appointmentDate);

    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}

export async function getAllAppointment(req, res) {
    try {

        let appointmentData = await Appointment.aggregate([{
            $sort: {
                startTime: 1
            }
        }, {
            $project: {
                yearMonthDay: {
                    $dateToString: {
                        format: "%Y-%m-%d",
                        date: "$startTime"
                    }
                },
                startTime: 1,
                endTime: 1
            }
        }, {
            $group: {
                _id: "$yearMonthDay",
                appointments: {
                    $push: {
                        startTime: "$$ROOT.startTime",
                        endTime: "$$ROOT.endTime"
                    }
                }
            }
        }])

        appointmentData = appointmentData.map(appointment => {
            let appointmentDate = getAvailableDate(appointment.appointments)
            appointment.appointments = appointmentDate

            return appointment
        });

        res.successWithData(appointmentData);

    } catch (error) {
        console.log(error);
        res.serverError("Something went wrong...");
    }
}