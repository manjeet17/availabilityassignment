import config from "../../config";
import {
    addAppointment,
    getAppointmentForDay,
    getAllAppointment
} from "./controllers/appointment";

var router = require('express').Router();

router.post('/', addAppointment);

router.post('/availability', getAppointmentForDay);

router.get('/availability/all', getAllAppointment);

module.exports = router