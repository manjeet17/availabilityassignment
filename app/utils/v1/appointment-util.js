export function getAvailableDate(appointments) {
    let appointmentDate = [],
        obj = {};

    for (let i = 0; i < appointments.length; i++) {

        if (!Object.keys(obj).length) {
            obj = {
                startTime: appointments[i].startTime,
                endTime: appointments[i].endTime
            }
        } else if (new Date(obj.endTime).getTime() >= new Date(appointments[i].startTime).getTime()) {
            obj.endTime = appointments[i].endTime;
        } else {

            obj.startTime = new Date(obj.startTime).getTime();
            obj.endTime = new Date(obj.endTime).getTime();

            appointmentDate.push(obj);

            obj = {
                startTime: appointments[i].startTime,
                endTime: appointments[i].endTime
            }
        }
    }

    if (Object.keys(obj).length) {

        obj.startTime = new Date(obj.startTime).getTime();
        obj.endTime = new Date(obj.endTime).getTime();

        appointmentDate.push(obj)
    }

    return appointmentDate;
}