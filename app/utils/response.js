"use strict";

export function success(message) {
  return this.status(200).json({
    statusCode: 200,
    data: {
      message: message
    },
    error: {}
  });
}

export function successWithData(data) {
  this.status(200).json({
    statusCode: 201,
    data: data,
    error: {}
  });
}

export function validationFailed(message, title, errorCode) {
  this.status(400).json({
    statusCode: 400,
    data: {},
    error: {
      title: title,
      message: message,
      errorCode: errorCode
    }
  });
}

export function unauthorized(message, title) {
  this.status(401).json({
    statusCode: 401,
    data: {},
    error: {
      message: message,
      title: title
    }
  });
}

export function expiredSession(message, title) {
  this.status(402).json({
    statusCode: 402,
    data: {},
    error: {
      message: message,
      title: title
    }
  });
}

export function invalidSession(message, title) {
  this.status(403).json({
    statusCode: 403,
    data: {},
    error: {
      message: message,
      title: title
    }
  });
}

export function thirdPartyError(message) {
  this.status(500).json({
    statusCode: 500,
    data: {},
    error: {
      message: message
    }
  });
}

export function serverError(message, title) {
  this.status(500).json({
    statusCode: 501,
    data: {},
    error: {
      message: message,
      title: title
    }
  });
}
